<?php
session_start();
include './model/data.php';
include './controller/debug.php';


if (!isset($_SESSION['user'])) {
    header('Location:index.php');
    die();
}

$req = $bdd->prepare('SELECT * FROM user WHERE token_user = ?');
$req->execute(array($_SESSION['user']));
$data = $req->fetch();

$userId = $data['id'];


$userDatas = getUserDatas($userId);
$userArchives = getUserArchive($userId);

$count = $bdd->prepare('SELECT COUNT(archive.id_archive) as archive FROM `archive` 
INNER JOIN `user_archive` ON user_archive.id_archive = archive.id_archive 
WHERE user_archive.id_user = :userId');
$count->bindValue(':userId', $userId, PDO::PARAM_INT);
$count->execute();
$tcount = $count->fetchAll(PDO::FETCH_ASSOC);
$tcount = array_reverse($tcount);

@$page = $_GET['page'];

if (empty($page)) {
    $page = 1;
}
$nbrElemsPages = 3;
$nbrPages = ceil($tcount[0]["archive"] / $nbrElemsPages);

$debut = ($page - 1) * $nbrElemsPages;


$req = $bdd->prepare('SELECT archive.date, archive.poids, archive.taille, archive.profil, archive.imc, archive.meta_base, archive.meta_actif FROM `archive` 
INNER JOIN `user_archive` ON user_archive.id_archive = archive.id_archive 
WHERE user_archive.id_user = :userId ORDER BY archive.date DESC LIMIT :debut, :nbrElemsPages ');
$req->bindValue(':userId', $userId, PDO::PARAM_INT);
$req->bindValue(':debut', $debut, PDO::PARAM_INT);
$req->bindValue(':nbrElemsPages', $nbrElemsPages, PDO::PARAM_INT);
$req->execute();
$tab = $req->fetchAll(PDO::FETCH_ASSOC);


$req2 = $bdd->prepare('SELECT archive.date, archive.poids, archive.taille, archive.profil, archive.imc, archive.meta_base, archive.meta_actif FROM `archive` 
INNER JOIN `user_archive` ON user_archive.id_archive = archive.id_archive 
WHERE user_archive.id_user = :userId ORDER BY archive.date DESC');
$req2->bindValue(':userId', $userId, PDO::PARAM_INT);
$req2->execute();
$tabmobile = $req2->fetchAll(PDO::FETCH_ASSOC);



?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Bienvenue sur My Life My Diet</title>
</head>

<body>

    <div class="topnav">

        <!-- Navigation links (hidden by default) -->
        <div id="myLinks">
            <a href="#infos">Mes informations</a>
            <a href="#imc">IMC</a>
            <a href="#resultat">Resultat</a>
            <a href="#archives">Archives</a>
            <a href="#aide">Besoin d'un coup de pouce?</a>
            <a href="#contact">Contact</a>
            <a href="./controller/deconnexion.php">Logout</a>
        </div>
        <!-- "Hamburger menu" / "Bar icon" to toggle the navigation links -->
        <a href="javascript:void(0);"aria-label="ouvrir le menu" class="icon" onclick="myFunction()"><img class="burger" src="./mymenuburger9.svg" alt="">

        </a>
    </div>
    <div class="titrelogo-landing">
        <img class="logogrand" src="./logomy1.svg" alt="logo">
        <h1>My Life My Diet</h1>


    </div>
    <div class="container-full-landing">
        <div class="touctouc">
            <div class="customerInfo" id="infos">
                <div class="info">
                    <h2>Mes informations</h2>
                    <div class="toggleDarkMode">
                        <label class="switch" for="input">
                            <input type="checkbox" id="input" name='input' onclick="darkmode()">
                          
                            <span class="slider round"></span>
                         </label>
                        <p>Thème sombre/clair
                        <p>
                    </div>
                    <p><?= $data['prenom'] ?> <?= $data['nom'] ?></p>
                    <p> <?= $data['naissance'] ?></p>
                    <p><?= $data['mail'] ?></p>
                    <p><?= $data['sexe'] ?></p>
                </div>
                <!-- Trigger/Open The Modal -->
                <div class="bouton-info">
                    <a class="btn-submit " id="myBtn">Modifier mes informations</a>

                    <a class="btn-submit" href="./controller/suppcompte.php">Supprimer mon compte</a>
                </div>
            </div>
            <!-- The Modal -->
            <div id="myModal" class="modal">
                <div class="modal-content" id="modal-content">
                    <span class="close" id="close">&times;</span>
                    <h2>Modifier mes informations</h2>
                    <div class="wrapper">
                        <form action="./controller/infochange.php" method="post">

                            <div class="box-wrapper">
                                <div class="form__group">
                                    <input type="date" name="naissance" class="form__control" value="<?= $data['naissance'] ?>">
                                    <span class="separator"> </span>
                                </div>
                                <div class="form__group">
                                    <input type="email" name="mail" class="form__control" value="<?= $data['mail'] ?>" required="required">
                                    <span class="separator"> </span>
                                </div>
                                <div class="form__group">
                                    <p>Vous etes: <abbr>*</abbr></p>
                                    <div class="radio-item">
                                        <input type="radio" id="femme" name="sexe" class="form__control__radio" value="femme" required="required">
                                        <label for="femme" class="form__label">une femme </label>
                                    </div>
                                    <span class="separator"> </span>
                                    <div class="radio-item">
                                        <input type="radio" id="homme" name="sexe" class="form__control__radio" value="homme" required="required">
                                        <label for="homme" class="form__label">un homme </label>

                                    </div>
                                    <span class="separator"> </span>
                                </div>
                                <div class="form__group">
                                    <button type="submit" class="btn-submit">Valider</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper wrap-landing">
            <form action="./controller/archive.php" class="login-form" method="post">
                <h2 class="text-center heading-mb">Calculateur d'IMC</h2>
                <div class="box-wrapper">
                    <div class="poidsTaille" id="imc">
                        <div class="form__group">
                            <input type="text" name="poids" class="form__control" placeholder="Entrez votre poids en kg" required="required" autocomplete="off">

                            <span class="separator"> </span>
                        </div>
                        <div class="form__group">
                            <input type="text" name="taille" class="form__control" placeholder="Entrez votre taille en cm" required="required" autocomplete="off">

                            <span class="separator"> </span>
                        </div>
                    </div>
                    <div class="form__group">
                        <p>Etes-vous plutôt : </p>
                        <div class="profil">
                            <div class="col1">
                                <div class="radio-item">
                                    <input type="radio" id="sedentaire" name="profil" class="form__field" value="Sédentaire" required="required">
                                    <label for="sedentaire" class="form__label">Sédentaire </label>
                                    <div class="text-ident">
                                        <p>si vous avez un travail de bureau ou une faible dépense sportive</p>
                                    </div>
                                </div>
                                <div class="radio-item">
                                    <input type="radio" id="legerementActif" name="profil" class="form__field" value="Légèrement actif" required="required">
                                    <label for="legerementActif" class="form__label">Légèrement Actif</label>
                                    <div class="text-ident">
                                        <p>si vous vous entraînez 1 à 3 fois par semaine</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col2">
                                <div class="radio-item">
                                    <input type="radio" id="actif" name="profil" class="form__field" value="Actif" required="required">
                                    <label for="actif" class="form__label">Actif </label>
                                    <div class="text-ident">
                                        <p> si vous vous entraînez 4 à 6 fois par semaine</p>
                                    </div>
                                </div>

                                <div class="radio-item">
                                    <input type="radio" id="tresActif" name="profil" class="form__field" value="Très actif" required="required">
                                    <label for="tresActif" class="form__label">Très Actif </label>

                                    <div class="text-ident">
                                        <p>si vous faites quotidiennement du sport ou des exercices physiques très soutenus</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="form-group action-wrapper action-land" id="resultat">

                    <button type="submit" class="btn-submit">Calculez votre IMC</button>
                </div>
                <div class="resultat">
                    <?php if ($userArchives == null) { ?>

                        <p>Faites le calcul de votre Imc aujourd'hui</p>
                    <?php } else { ?>
                        <p>Votre Imc est de: <span><?= $tab[0]['imc'] ?></span>.
                        <p>
                            <?php if ($tab[0]['imc'] <= 18.4) { ?>
                        <p>Vous avez une corpulence <span>maigre</span>.</p>
                    <?php
                            } else if ($tab[0]['imc'] >= 18.5 && $tab[0]['imc'] <= 24.9) { ?>
                        <p>Vous avez une corpulence <span>normale</span>.</p>
                    <?php
                            } else if ($tab[0]['imc'] >= 25 && $tab[0]['imc'] <= 29.9) { ?>
                        <p>Vous êtes en <span>surpoids</span>.</p>
                    <?php
                            } else if ($tab[0]['imc'] >= 30 && $tab[0]['imc'] <= 34.9) { ?>
                        <p>Vous êtes en <span>obésité modérée</span>.</p>
                    <?php
                            } else if ($tab[0]['imc'] >= 35 && $tab[0]['imc'] <= 39.9) { ?>
                        <p>Vous êtes en <span>obésité sévère</span>.</p>
                    <?php
                            } else if ($tab[0]['imc'] >= 40) { ?>
                        <p>Vous êtes en <span>obésité morbide</span>.</p>
                    <?php
                            }

                    ?>
                    <p>Votre métabolisme de base au repos est de: <span><?= $tab[0]['meta_base'] ?> Kcal</span>.</p>
                    <p>Votre métabolisme actif est de: <span><?= $tab[0]['meta_actif'] ?> Kcal</span>.


                    <p class="retour">Vos besoins calorifiques sont de: <span><?= $tab[0]['meta_actif'] ?> Kcal</span> par jour.</p>

                <?php }
                ?>


                </div>
            </form>

        </div>
    </div>
    <div id="archives">
        <?php if ($userArchives != null) {
        ?>
            <div class="suivi-mobile cache-mobile">
                <h2>Suivi</h2>
                <?php for ($i = 0; $i < count($tabmobile); $i++) {
                    $date = date("d/m/Y H:i", strtotime($tabmobile[$i]['date'])); ?>
                    <button class="accordion"><div class="date-acc"><?= $date ?></div></button>
                    <div class="panel">
                        <p>Poids: <?= $tabmobile[$i]["poids"] ?></p>
                        <p>Taille: <?= $tabmobile[$i]["taille"] ?></p>
                        <p>Profil: <?= $tabmobile[$i]["imc"] ?></p>
                        <p>Profil: <?= $tabmobile[$i]["profil"] ?></p>
                        <p>Métabolisme de base: <?= $tabmobile[$i]["meta_base"] ?></p>
                        <p>Métabolisme actif: <?= $tabmobile[$i]["meta_actif"] ?></p>
                        <p>Besoin: <?= $tabmobile[$i]["meta_actif"] ?></p>
                    </div>
                <?php } ?>

            </div>
            <?php if ($userArchives !== null) {
            ?>
                <div class="suivi cache">
                    <h2>Suivi</h2>
                    <div class="table-wrapper-data">
                        <table>
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Poids</th>
                                    <th>Taille</th>
                                    <th>Imc</th>
                                    <th>Profil</th>
                                    <th>Métabolisme de base</th>
                                    <th>Métabolisme actif</th>
                                    <th>Besoins</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php for ($i = 0; $i < count($tab); $i++) {
                                    $date = date("d/m/Y H:i", strtotime($tab[$i]['date']));

                                ?>
                                    <tr>
                                        <td class="no-wrapp"><?= $date ?></td>
                                        <td class="no-wrapp"><?= $tab[$i]["poids"] ?></td>
                                        <td class="no-wrapp"><?= $tab[$i]["taille"] ?></td>
                                        <td class="no-wrapp"><?= $tab[$i]["imc"] ?></td>
                                        <td class="no-wrapp"><?= $tab[$i]["profil"] ?></td>
                                        <td class="no-wrapp"><?= $tab[$i]["meta_base"] ?></td>
                                        <td class="no-wrapp"><?= $tab[$i]["meta_actif"] ?></td>
                                        <td class="no-wrapp"><?= $tab[$i]["meta_actif"] ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>

                        </table>
                    </div>

                </div>


                <div id="pagination">
                    <nav class="pagination-container">
                        <button class="pagination-button" id="prev-button" aria-label="Previous page" title="Previous page">
                            &lt;
                        </button>
                        <?php
                        for ($i = 1; $i <= $nbrPages; $i++) {
                            if ($page != $i) {
                                echo "<a class='page-link' href='?page=$i'>$i</a>";
                            } else {
                                echo "<a class='page-link-active link'>$i</a>";
                            }
                        }
                        ?>
                        <button class="pagination-button" id="next-button" aria-label="Next page" title="Next page">
                            &gt;
                        </button>

                    <?php } ?>
                    </nav>
                </div>
            <?php } ?>

    </div>




    <div class="callToAction" id="aide">
        <img src="./pexels-andres-ayrton-6551415.jpg" alt="appelez nous">
        <div class="callTextBut">
            <p>Vous voulez améliorer votre santé, reprendre le sport ou changer vos habitudes alimentaires?
            </p>
            <p>Les coachs de My life My Diet vous aident à atteindre vos objectifs en vous fournissant des conseils personnalisés et en vous motivant.
                N’hesitez plus

            </p>
            <a class="btn-submit" href="tel:0606060606">Appelez-nous</a>
        </div>
    </div>




    <?php
    include('footer.php') ?>


    <script>
        // =====================Modaux=================
        var modal = document.getElementById("myModal");
        var btn = document.getElementById("myBtn");
        var span = document.getElementsByClassName("close")[0];



        btn.onclick = function() {
            modal.style.display = "block";
        }


        span.onclick = function() {
            modal.style.display = "none";

        }

        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
        // ================accordeon==================
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }
        // ===============menu burger==================
        function myFunction() {
            var x = document.getElementById("myLinks");
            if (x.style.display === "block") {
                x.style.display = "none";
            } else {
                x.style.display = "block";
            }
        }

        function darkmode() {
            var element = document.body;
            element.classList.toggle("dark-mode");
        }
    </script>
</body>

</html>