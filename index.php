<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style.css">
    <title>Connexion - My</title>
</head>



<body>
    <main>


        <div class="container-full">

            <div class="wrapper-form">
                <div class="logoTitre">
                    <img class='logo' src="logomy1.svg" alt="logo">
                    <h1>My Life My Diet</h1>
                </div>
                <div class="wrapper">
                    <?php
                    if (isset($_GET['compte_supp'])) {
                    ?>
                        <div class="alert-ok">
                            <p>
                                Votre compte a bien été supprimé!
                            </p>
                        </div>
                       
                     <?php }


                    if (isset($_GET['login_err'])) {
                        $err = htmlspecialchars($_GET['login_err']);

                        switch ($err) {
                            case 'password':
                        ?>
                                <div class="alert">
                                    <p>
                                        <strong>Erreur :</strong> mot de passe incorrect
                                    </p>
                                </div>
                            <?php
                                break;

                            case 'mail':
                            ?>
                                <div class="alert">
                                    <p>
                                        <strong>Erreur :</strong> email incorrect
                                    </p>
                                </div>
                            <?php
                                break;

                            case 'already':
                            ?>
                                <div class="alert">
                                    <p>
                                        <strong>Erreur :</strong> compte inexistant
                                    </p>
                                </div>
                    <?php
                                break;
                        }
                    } ?>
                    <form action="./controller/connexion.php" class="login-form" method="post">
                        <div class="text-center"></div>
                        <div class="form__group">
                            <input type="mail" name="mail" class="form__control" placeholder="Email" required="required" autocomplete="off">

                            <span class="separator"> </span>
                        </div>
                        <div class="form__group">
                            <input type="password" name="password" class="form__control" placeholder="Mot de passe" required="required" autocomplete="off">
                            <span class="separator"> </span>
                        </div>
                        <div class="form__group action-wrapper">
                            <a class="btn-submit" href="inscription.php">Inscription</a>
                            <button type="submit" class="btn-submit">Connexion</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <?php
        include('footer.php') ?>
    </main>
</body>

</html>