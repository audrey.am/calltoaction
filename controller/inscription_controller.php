<?php
require_once '../model/data.php'; // On inclu la connexion à la bdd
include 'debug.php';

// Si les variables existent et qu'elles ne sont pas vides
if (!empty($_POST['prenom']) && !empty($_POST['nom']) && !empty($_POST['naissance']) && !empty($_POST['mail']) && !empty($_POST['sexe']) && !empty($_POST['password']) && !empty($_POST['password_retype'])) {
    // Patch XSS
    $prenom = htmlspecialchars($_POST['prenom']);
    $nom = htmlspecialchars($_POST['nom']);
    $naissance = htmlspecialchars($_POST['naissance']);
    $mail = htmlspecialchars($_POST['mail']);
    $sexe = htmlspecialchars($_POST['sexe']);
    $password = htmlspecialchars($_POST['password']);
    $password_retype = htmlspecialchars($_POST['password_retype']);


    // On vérifie si l'utilisateur existe
    $check = $bdd->prepare('SELECT prenom, nom, mail FROM user WHERE mail = ?');
    $check->execute(array($mail));
    $data = $check->fetch();
    $row = $check->rowCount();

    $mail = strtolower($mail); // on transforme toute les lettres majuscule en minuscule pour éviter que Foo@gmail.com et foo@gmail.com soient deux compte différents ..

    // Si la requete renvoie un 0 alors l'utilisateur n'existe pas 
    if ($row == 0) {
        if (strlen($prenom) <= 40) { // On verifie que la longueur du prenom <= 40
            if (strlen($nom) <= 40) {
                if (strlen($mail) <= 40) {
                    if (filter_var($mail, FILTER_VALIDATE_EMAIL)) { // Si l'mail est de la bonne forme

                        if ($password === $password_retype) { // si les deux mdp saisis sont bon

                            // On hash le mot de passe avec Bcrypt, via un coût de 12
                            $cost = ['cost' => 12];
                            $password = password_hash($password, PASSWORD_BCRYPT, $cost);

                            // On insère dans la base de données
                            $insert = $bdd->prepare("INSERT INTO user (prenom, nom, naissance, mail, password, sexe, token_user) 
                            VALUES (:prenom, :nom, :naissance, :mail, :password, :sexe, :token_user) 
                            ");
                            $insert->execute(array(
                                'prenom' => $prenom,
                                'nom' => $nom,
                                'naissance' => $naissance,
                                'mail' => $mail,
                                'password' => $password,
                                'sexe' => $sexe,
                                'token_user' => bin2hex(openssl_random_pseudo_bytes(64))


                            ));
                            // On redirige avec le message de succès
                            header('Location: ../index.php?success=success');
                            die();}
                        } else {
                            header('Location: ../inscription.php?reg_err=password');
                            die();
                        }
                    } else {
                        header('Location: ../inscription.php?reg_err=mail');
                        die();
                    }
                } else {
                    header('Location: ../inscription.php?reg_err=mail_length');
                    die();
                }
            } else {
                header('Location: ../inscription.php?reg_err=nom_length');
                die();
            }
        } else {
            header('Location: ../inscription.php?reg_err=prenom_length');
            die();
        }
    } else {
        header('Location: ../inscription.php?reg_err=already');
        die();
    }

