<?php
session_start();
require_once './debug.php';
require '../model/data.php';
if (!isset($_SESSION['user'])) {
    header('Location:./index.php');
    die();
}
$req = $bdd->prepare('SELECT * FROM user WHERE token_user = ?');
$req->execute(array($_SESSION['user']));
$data = $req->fetch();
// Je stocke l'id du client
$userId = $data['id'];


if (!empty($_POST['naissance']) && !empty($_POST['mail']) && !empty($_POST['sexe'])) {
    // Patch XSS

    $naissance = htmlspecialchars($_POST['naissance']);
    $mail = htmlspecialchars($_POST['mail']);
    $sexe = htmlspecialchars($_POST['sexe']);
    $mail = strtolower($mail); // on transforme toutes les lettres majuscules en minuscules pour éviter que Foo@gmail.com et foo@gmail.com soient deux comptes différents ..



    updateInfoFromOneClient($naissance, $mail, $sexe, $userId);
    header('Location: ../landing.php');
} else {
    echo 'bug';
}