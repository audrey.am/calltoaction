<?php
session_start();
require_once '../model/data.php';
require './debug.php';


$req = $bdd->prepare('SELECT * FROM user WHERE token_user = ?');
$req->execute(array($_SESSION['user']));
$data = $req->fetch();


$userId = $data['id'];

$userDatas = getUserDatas($userId);

$req2 = $bdd->prepare('SELECT * FROM archive INNER JOIN user_archive WHERE user_archive.id_user = ?');
$req2->execute(array($userId));
$datarchive = $req2->fetch();


if (!empty($_POST['poids']) && !empty($_POST['taille']) && !empty($_POST['profil'])) {
    $poids = htmlspecialchars($_POST['poids']);
    $taille = htmlspecialchars($_POST['taille']);
    $profil = htmlspecialchars($_POST['profil']);
    $aujourdhui = Date("Y-m-d");
    $age = date_diff(date_create($data['naissance']), date_create($aujourdhui));
   

$tailleEnM = $taille/100;



    $imc = $poids / (($tailleEnM) * ($tailleEnM));

if ($data['sexe'] == 'femme'){
    $meta_base = (9.740 * $poids) + (172.9 *($tailleEnM) - (4.737 * $age->format('%y') ) +667.051);

}else{
    $meta_base = (13.707 * $poids) + (492.3 *($tailleEnM) - (6.673 * $age->format('%y') ) +77.607);

}

if ($profil == 'Sédentaire'){
    $meta_actif= $meta_base * 1.2;

}else if ($profil == 'Légèrement actif'){
    $meta_actif= $meta_base * 1.375;
}else if ($profil == 'Actif'){
    $meta_actif= $meta_base * 1.55;
}else if($profil == 'Très actif'){
    $meta_actif= $meta_base * 1.725;
}

   
    



    // On insère dans la base de données
    $insert = $bdd->prepare("INSERT INTO archive (poids, taille, profil, imc, meta_base, meta_actif) 
                            VALUES (:poids, :taille, :profil, :imc, :meta_base, :meta_actif) 
                            ");
    $insert->execute(array(
        'poids' => $poids,
        'taille' => $taille,
        'profil' => $profil,
        'imc' => $imc,
        'meta_base' => $meta_base,
        'meta_actif' => $meta_actif,
       
    ));
    print $id_archive = $bdd->lastInsertId();


    $req = $bdd->prepare("INSERT into user_archive (id_user, id_archive) values (?, ?)");
    $req->execute([$userId, $id_archive]);

    // On redirige avec le message de succès
    header('Location: ../landing.php');
    die();
} else {
    header('Location: ../landing.php?reg_err=donnees');
    die();
}


