<?php
session_start();
require_once '../model/data.php';

if (!empty($_POST['mail']) && !empty($_POST['password'])) 
{
  
    $mail = htmlspecialchars($_POST['mail']);
    $password = htmlspecialchars($_POST['password']);

    $mail = strtolower($mail);

   
    $check = $bdd->prepare('SELECT mail, password, token_user FROM user WHERE mail = ?');
    $check->execute(array($mail));
    $data = $check->fetch();
    $row = $check->rowCount();



  
    if ($row > 0) {
       
        if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {
           
            if (password_verify($password, $data['password'])) {
                
                $_SESSION['user'] = $data['token_user'];
                header('Location: ../landing.php');
                die();
            } else {
                header('Location: ../index.php?login_err=password');
                die();
            }
        } else {
            header('Location: ../index.php?login_err=mail');
            die();
        }
    } else {
        header('Location: ../index.php?login_err=already');
        die();
    }
} else {
    header('Location: ../index.php');
    die();
} 