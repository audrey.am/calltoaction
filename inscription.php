<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style.css">
    <title>Inscription - MY</title>
</head>

<body>
<a class="logo-retour" href="index.php"><img class="logo-retour" src="arrow-left-solid.svg" alt=""></a>

    <div class="container-full">
        <div class="wrapper-form">
            <div class="logoTitre">
                <img class='logo' src="logomy1.svg" alt="logo">
                <h1>My Life My Diet</h1>
            </div>
            <div class="wrapper">
                <?php
                if (isset($_GET['reg_err'])) {
                    $err = htmlspecialchars($_GET['reg_err']);

                    switch ($err) {
                        case 'password':
                ?>
                            <div class="alert">
                                <p>
                                    <strong>Erreur</strong> mot de passe différent
                                </p>
                            </div>
                        <?php
                            break;

                        case 'email':
                        ?>
                            <div class="alert">
                                <p>
                                    <strong>Erreur</strong> email non valide
                                </p>
                            </div>
                        <?php
                            break;

                        case 'email_length':
                        ?>
                            <div class="alert">
                                <p>
                                    <strong>Erreur</strong> nombre maximum de caractères : Email
                                </p>
                            </div>
                        <?php
                            break;

                        case 'prenom_length':
                        ?>
                            <div class="alert">
                                <p>
                                    <strong>Erreur</strong> nombre maximum de caractères : Prenom
                                </p>
                            </div>
                        <?php
                            break;

                        case 'nom_length':
                        ?>
                            <div class="alert">
                                <p>
                                    <strong>Erreur</strong> nombre maximum de caractères : Nom
                                </p>
                            </div>
                        <?php
                            break;

                        case 'already':
                        ?>
                            <div class="alert">
                                <p>
                                    <strong>Erreur</strong> compte deja existant
                                </p>
                            </div>
                <?php
                            break;
                    }
                }
                ?>

                <form action="./controller/inscription_controller.php" class="login-form" method="post">
                    <h2 class="text-center heading-mb">Inscription</h2>
                    <div class="box-wrapper">
                        <div class="box-1">
                            <div class="form__group">
                                <input type="text" name="prenom" class="form__control" placeholder="Prenom *" required="required" autocomplete="off">

                                <span class="separator"> </span>
                            </div>
                            <div class="form__group">
                                <input type="text" name="nom" class="form__control" placeholder="Nom *" required="required" autocomplete="off">

                                <span class="separator"> </span>
                            </div>
                            <div class="form__group">
                                <input type="text" name="naissance" class="form__control" placeholder="Date de naissance *" onfocus="(this.type='date')" required="required" autocomplete="off">

                                <span class="separator"> </span>
                            </div>
                            <div class="form__group">
                                <input type="mail" name="mail" class="form__control" placeholder="Adresse mail *" required="required" autocomplete="off">

                                <span class="separator"> </span>
                            </div>
                            <div class="form__group">
                                <p>Vous etes: <abbr>*</abbr></p>
                                <div class="radio-item">



                                    <input type="radio" id="femme" name="sexe" class="form__control__radio" value="femme" required="required" autocomplete="off">
                                    <label for="femme" class="form__label">une femme </label>
                                </div>
                                <span class="separator"> </span>
                                <div class="radio-item"> <input type="radio" id="homme" name="sexe" class="form__control__radio" value="homme" required="required" autocomplete="off">
                                    <label for="homme" class="form__label">un homme </label>

                                </div>
                                <span class="separator"> </span>
                            </div>
                            <div class="form__group">
                                <input type="password" name="password" class="form__control" required="required" placeholder="Mot de passe *" autocomplete="off">

                                <span class="separator"> </span>
                            </div>
                            <div class="form__group">
                                <input type="password" name="password_retype" class="form__control" required="required" placeholder="Confirmez le mot de passe *" autocomplete="off">

                                <span class="separator"> </span>
                            </div>

                        </div>
                    </div>
                    <div class="form__group action-wrapper">

                        <button type="submit" class="btn-submit">Inscription</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php
    include('footer.php') ?>
</body>

</html>