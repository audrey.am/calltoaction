<?php



$host = 'localhost';
$db = 'cta';
$user = 'utilisateur';
$pass = 'utilisateur';

try {
    $bdd = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // mode de fetch par défaut : FETCH_ASSOC / FETCH_OBJ / FETCH_BOTH
    $bdd->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    echo $e->getMessage();
}

function getUserPastEvents($userId)
{
    $tableauPasse = array();
    $date = new DateTime;
    $date = date_format($date, "Y-m-d H:i:s");
    global $bdd;
    $reqTabPres = $bdd->prepare('SELECT * from user 
    INNER JOIN user_archive on user.id = user_archive.id_user 
    INNER JOIN archive on archive.id_archive = user_archive.id_archive
    WHERE user.id = :userId 
    ORDER BY archive.date ASC');

    $reqTabPres->bindValue(':userId', $userId, PDO::PARAM_INT);
    $reqTabPres->execute();
    $tabPres = $reqTabPres->fetchAll(PDO::FETCH_ASSOC);


    for ($u = 0; $u < count($tabPres); $u++) {
        if ($tabPres[$u]['periodStart'] < $date) {
            array_push($tableauPasse, $tabPres[$u]);
        }
    }
    return $tableauPasse;
};




function getUserDatas($userId)
{
    global $bdd;
    $req = $bdd->prepare('SELECT prenom, nom, mail, naissance, sexe,token_user FROM `user` WHERE id = ?');
    $req->execute(array($userId));
    return $req->fetchAll(PDO::FETCH_ASSOC);
};

function updateInfoFromOneClient($naissance, $mail, $sexe, $userId)
{
    global $bdd;
    $req = $bdd->prepare('UPDATE user SET naissance = ?, mail = ?, sexe = ? WHERE id = ?');
    $req->execute(array($naissance, $mail, $sexe, $userId));
    return $req->fetchAll(PDO::FETCH_ASSOC);
};
function suppcompte($userId)
{
    global $bdd;
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $req = $bdd->prepare('DELETE FROM archive WHERE id_archive IN (SELECT id_archive FROM user_archive WHERE id_user = :id_user);');
    $req->bindParam(':id_user', $userId, PDO::PARAM_INT);
    $req->execute();
    $req1 = $bdd->prepare('DELETE FROM user_archive WHERE id_user = :userId');
    $req1->bindParam(':userId', $userId, PDO::PARAM_INT);
    $req1->execute();
    $req2 = $bdd->prepare('DELETE FROM user WHERE id = :userId');
    $req2->bindParam(':userId', $userId, PDO::PARAM_INT);
    $req2->execute();
}

function getUserArchive($userId){
    global $bdd;
    $req2 = $bdd->prepare('SELECT * FROM archive INNER JOIN user_archive WHERE user_archive.id_user = ?');
    $req2->execute(array($userId));
    return $req2->fetchAll(PDO::FETCH_ASSOC);
}
